#! /bin/bash

F=$1

[ -n "$F" ] || (
    echo "USAGE: ${0##/} [testfile]" >&2
    exit 2
)

cleanup() {
    rm -f "$F"
}

trap cleanup EXIT

blocks_used() {
    echo $(($(stat -c "%b * %B" "$1") / BLOCK_SIZE))
}

pwrite() {
    xfs_io -c "pwrite -q $1b $2b" "$F"
    USED=$(blocks_used "$F")
}

pwrite_check() {
    [ $# -ge 3 ] || set -- "$1" "$2" 0

    USED_BEFORE=$USED
    pwrite "$1" "$2"
    if ((USED_BEFORE + $2 + $3 != USED)); then
	echo pwrite: $USED_BEFORE + $2 + $3 == $((USED_BEFORE + $2 + $3)) != $USED
	trap - EXIT
	exit 1
    fi
}

punch_hole() {
    xfs_io -c "fpunch $1b $2b" "$F"
    USED=$(blocks_used "$F")
}

punch_hole_check() {
    [ $# -ge 3 ] || set -- "$1" "$2" 0

    USED_BEFORE=$USED
    punch_hole "$1" "$2"
    if ((USED_BEFORE - $2 - $3 != USED)); then
	echo punch_hole: $USED_BEFORE - $2 - $3 == $((USED_BEFORE - $2 - $3)) != $USED
	trap - EXIT
        exit 1
    fi
    pwrite_check "$1" "$2" "$3"
}

xfs_io -c "open -ft $F"
BLOCK_SIZE=$(stat -f -c "%S" "$F")
IN=$(((BLOCK_SIZE - 232) / 8))
DI=$(((BLOCK_SIZE - 24) / 8))
echo IN=$IN DI=$DI

pwrite 0 3
punch_hole_check 0 1
punch_hole_check 1 1
punch_hole_check 2 1
punch_hole_check 0 2
punch_hole_check 1 2
punch_hole_check 0 3

pwrite 0 $((IN * DI))
punch_hole_check 0 1
punch_hole_check 1 1
punch_hole_check $((IN * DI - 2)) 1
punch_hole_check $((IN * DI - 1)) 1

punch_hole_check 0 $DI 1
punch_hole_check $DI $DI 1
punch_hole_check $(((IN - 1) * DI)) $DI 1
punch_hole_check $(((IN - 2) * DI)) $DI 1

punch_hole_check 0 $((IN * DI)) $IN

pwrite 0 $((DI * DI))
punch_hole_check 0 1
punch_hole_check 0 $DI 1
punch_hole_check $DI $DI 1
punch_hole_check $(((IN - 1) * DI)) $DI 1
punch_hole_check $(((IN - 2) * DI)) $DI 1
punch_hole_check 0 $((DI * DI)) $((DI + 1))
